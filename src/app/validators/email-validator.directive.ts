import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';

@Directive({
  selector: '[emailValidation]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: EmailValidatorDirective,
    multi: true
  }]
})
export class EmailValidatorDirective implements Validator {

  constructor() { }
  validate(control: AbstractControl): ValidationErrors {
    return this.emailCheck(control.value) ? null : {'invalid': true}
  }

  emailCheck(email: string): boolean {
    // when page is loaded email == null and to ignore the email check return false;
    if (email == null) {
      return false;
    }
    // check email with regex
    const reg = /^[a-z0-9._%+-]+@[a-z0-9._-]+\.[a-z]{2,4}$/;
    return reg.exec(email.toLowerCase()) != null;;
  }

}
