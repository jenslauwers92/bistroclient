import { Component, OnInit } from '@angular/core';
import {MenuService} from '../services/menu.service';
import {Dish} from '../models/dish/dish';
import {Drink} from '../models/drink/drink';
import {HttpErrorResponse} from '@angular/common/http';
import {RestaurantService} from '../services/restaurant.service';
import {Restaurant} from '../models/restaurant/restaurant';
import {OrderItem} from '../models/orderItem/order-item';
import {OrderItemService} from '../services/order-item.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DrinkType} from '../enums/drink-type.enum';
import {DishType} from '../enums/dish-type.enum';
import {AccountDetails} from '../models/accountDetails/account-details';
import {AuthenticationService} from '../services/authentication.service';
import {AccountDetailsService} from '../services/account-details.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  // menu: Menu;
  public dishes: Dish[] = [];
  public drinks: Drink[] = [];
  public drinkTypes = DrinkType;
  public dishTypes = DishType;
  public restaurantId; // test for restaurantid
  public menuId;
  public tempOrderItemId = 0;
  public isListLoading = false;
  public isDrinkListLoading = false;
  public restaurant: Restaurant;
  public confirmedOrder = false;
  public orderItemList: OrderItem [] = [];
  public totalAmount = 0;
  private showLoading: false;
  user = this.authentication.getUserFromLocalCache();
  credits: number;
  userName = this.user.username;
  refreshedUserDetails: AccountDetails;
  notEnoughMoney = false;

  constructor(private menuService: MenuService,
              private restaurantService: RestaurantService,
              private orderItemService: OrderItemService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private authentication: AuthenticationService,
              private accountDetails: AccountDetailsService) { }

  ngOnInit(): void {
    this.restaurantId = this.activatedRoute.snapshot.paramMap.get('id');
    this.getRestaurant();
    this.getUserDetails();
    console.log('actual restaurant: ' + this.restaurant);
    console.log('actual menu id: ' + this.menuId);
  }

  getRestaurant(): void {
    this.restaurantService.getRestaurantById(this.restaurantId).subscribe(restaurant => { // subscribe voert methode uit
      this.restaurant = restaurant;
      this.menuId = this.restaurant.menu.id;
      this.loadDrinks();
      this.loadDishes();
    });
  }

  loadDrinks(): void {
    this.isDrinkListLoading = true;
    this.menuService.loadDrinksByType(this.menuId).subscribe( (data: Drink[]) => {
        this.drinks = data;
        this.isDrinkListLoading = false;
      },
      (errorResponse: HttpErrorResponse) => {
        if (errorResponse instanceof HttpErrorResponse) {
          if (errorResponse.error instanceof ErrorEvent) {
            console.error('Error event');
          } else {
            switch (errorResponse.status) {
              case 403:
                this.router.navigateByUrl('/error/403');
                break;
              case 401:
                this.router.navigateByUrl('/login');
                break;
              case 404:
                this.router.navigate(['**']);
                break;
              case 500:
                this.router.navigateByUrl('/error/500');
            }
          }
        }
        console.log(errorResponse.error.message);
        this.isDrinkListLoading = false;
      });
  }

  loadDishes(): void {
    this.isListLoading = true;
    this.menuService.loadDishesByType(this.menuId).subscribe( (data: Dish[]) => {
        this.dishes = data;
        this.isListLoading = false;
        console.log(this.dishes);
      },
      (errorResponse: HttpErrorResponse) => {
        if (errorResponse instanceof HttpErrorResponse) {
          if (errorResponse.error instanceof ErrorEvent) {
            console.error('Error event');
          } else {
            switch (errorResponse.status) {
              case 403:
                this.router.navigateByUrl('/error/403');
                break;
              case 401:
                this.router.navigateByUrl('/login');
                break;
              case 404:
                this.router.navigate(['**']);
                break;
              case 500:
                this.router.navigateByUrl('/error/500');
            }
          }
        }
        console.log(errorResponse.error.message);
        this.isListLoading = false;
      });
  }

  onOrderItemCreate(amountString: string, dish: Dish , drink: Drink): void {
    this.tempOrderItemId += 1;
    const amount: number = +amountString;
    const orderItem = new OrderItem();
    orderItem.id = this.tempOrderItemId;
    orderItem.amount = amount;
    if (dish != null){
      orderItem.dish = dish;
      orderItem.price = dish.price * amount;
      this.totalAmount += orderItem.price;
    }
    if (drink != null){
      orderItem.drink = drink;
      orderItem.price = drink.price * amount;
      this.totalAmount += orderItem.price;
    }
    this.orderItemList.push(orderItem);
    console.log(this.orderItemList);

  }

  onDelete(selected: number): void{
    this.totalAmount -= this.orderItemList.find(orderItem => orderItem.id = selected).price;
    this.orderItemList = this.orderItemList.filter(obj => obj !== this.orderItemList.find(orderItem => orderItem.id = selected));
    console.log(this.orderItemList);
  }

  confirmOrder(): void {
    this.confirmedOrder = false;
    const orderItemList = this.orderItemList;
    orderItemList.forEach(orderItem => orderItem.id = undefined);
    this.orderItemList = orderItemList;
    console.log(this.orderItemList);
 //   if (this.checkMoney()){
    this.orderItemService.saveOrderItems(this.orderItemList).subscribe(
      (orderItems: OrderItem []) => {
        console.log(orderItems);
        this.confirmedOrder = true;
        this.orderItemList = [];
        const form = document.getElementById('orderForm') as HTMLFormElement;
        form.reset();
        setTimeout(() => {
          this.confirmedOrder = false;
          document.getElementById('closeButton').click();
        }, 2000);
        this.totalAmount = 0;
      }, (errorResponse: HttpErrorResponse) => {
        if (errorResponse instanceof HttpErrorResponse) {
          if (errorResponse.error instanceof ErrorEvent) {
            console.error('Error event');
          } else {
            switch (errorResponse.status) {
              case 403:
                this.router.navigateByUrl('/error/403');
                break;
              case 401:
                this.router.navigateByUrl('/login');
                break;
              case 404:
                this.router.navigate(['**']);
                break;
              case 500:
                this.router.navigateByUrl('/error/500');
            }
          }
        }
        console.log(errorResponse);
        this.showLoading = false;
      }
    );
//    } else{
     // this.notEnoughMoney = true;
    // }
  }

  checkMoney(): boolean {
    console.log(this.credits);
    console.log(this.totalAmount);
    return (this.credits * 10) < this.totalAmount;
  }

  arrayOne(n: number): any[] {
    return Array(n);
  }
  getUserDetails(): void {
    this.accountDetails.getCurrentAccountDetail(this.userName).subscribe(refreshedUserDetails => { // subscribe voert methode uit
      this.credits = refreshedUserDetails.credits;
      console.log(this.refreshedUserDetails);
      console.log(this.credits);
    });
  }
}
