import {Component, OnInit} from '@angular/core';
import {Feedback} from '../../models/feedback/feedback';
import {Restaurant} from '../../models/restaurant/restaurant';
import {RestaurantService} from '../../services/restaurant.service';
import {FeedbackService} from '../../services/feedback.service';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent implements OnInit {
  feedbacks: Feedback[];

  public feedback: Feedback;
  public rating: number;
  public review: string;
  public restaurant: Restaurant;
  submitted = false;
  ratingOk = false;

  constructor(public restaurantService: RestaurantService,
              public feedbackService: FeedbackService, private activatedRoute: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    this.restaurantService.getRestaurant(Number(this.activatedRoute.snapshot.paramMap.get('id')))
      .subscribe((restaurant: Restaurant) => {
        this.restaurant = restaurant;
      }, (errorResponse: HttpErrorResponse) => {
        if (errorResponse instanceof HttpErrorResponse) {
          if (errorResponse.error instanceof ErrorEvent) {
            console.error('Error event');
          } else {
            switch (errorResponse.status) {
              case 403:
                this.router.navigateByUrl('/error/403');
                break;
              case 401:
                this.router.navigateByUrl('/login');
                break;
              case 404:
                this.router.navigate(['**']);
                break;
              case 500:
                this.router.navigateByUrl('/error/500');
            }
          }
        }
        console.log(errorResponse.error.message);
      }
    );
    this.feedback = new Feedback();
  }


  addRating(rating: number): number {
    if (!rating) { return; }
    this.feedback.rating = rating;
    console.log(this.feedback.rating);
    if (this.rating !== 0) {
      this.ratingOk = true;
      return this.feedback.rating;
    }

  }

  addReview(review: string): string {
    if (!review) { return; }
    this.feedback.review = review;
    console.log(this.feedback.review);
    return this.feedback.review;
  }

  save(form: NgForm): void {
    this.feedbackService.saveFeedback(this.feedback, this.restaurant.id).subscribe((feedback: Feedback) => {
      this.router.navigate(['restaurant/', this.restaurant.id]); // TODO: give the right router path for restaurant list
    });
    console.log(this.feedback);
    this.submitted = true;
    form.reset();
    this.feedback = new Feedback();
  }
}
