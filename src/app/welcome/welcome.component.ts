import { Component, OnInit } from '@angular/core';
import {AdvertService} from '../services/advert.service';
import {Advert} from '../models/ads/advert';
import {HttpErrorResponse} from '@angular/common/http';
import {AuthenticationService} from '../services/authentication.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  adverts: Advert[] | HttpErrorResponse;
  public isLoggedIn = false;

  constructor(private advertService: AdvertService,
              private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
     this.getAds();
    // check if user is logged in
     this.authenticationService.isLoggedIn();
     this.authenticationService.isUserLoggedIn$.subscribe(loggedIn => this.isLoggedIn = loggedIn);
  }

  getAds(): void {
    this.advertService.getAdverts().subscribe(adverts => { // subscribe voert methode uit
      this.adverts = adverts;
      console.log(this.adverts);
    });
  }
}








