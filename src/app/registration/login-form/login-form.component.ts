import { Component, OnDestroy, OnInit } from '@angular/core';
import {AccountDetailsService} from '../../services/account-details.service';
import {AuthenticationService} from '../../services/authentication.service';
import {NgForm} from '@angular/forms';
import {User} from '../../models/user/user';
import {AccountDetails} from '../../models/accountDetails/account-details';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit, OnDestroy {
  user = new User();
  accountDetails = new AccountDetails();
  showLoading = false;
  loginFail = false;
  private subscriptions: Subscription[] = [];
  message: string;

  constructor(private authenticationService: AuthenticationService, private router: Router) { }

  ngOnInit(): void {
  }

  processForm(logForm: NgForm): void {
    this.showLoading = true;
    this.subscriptions.push(
      this.authenticationService.login(this.user).subscribe(
        (response: HttpResponse<User>) => {
          const token = response.headers.get('Jwt-Token');
          this.authenticationService.saveToken(token);
          this.authenticationService.addUserToLocalCache(response.body);
          this.authenticationService.isLoggedIn();
          this.showLoading = false;
          this.router.navigate(['account/details']);
          console.log(response);
        },
        (errorResponse: HttpErrorResponse) => {
          if (errorResponse instanceof HttpErrorResponse) {
            if (errorResponse.error instanceof ErrorEvent) {
              console.error('Error event');
            } else {
              switch (errorResponse.status) {
                case 400:
                  this.showErrorMessage(errorResponse.error.message);
                  break;
                case 403:
                  this.router.navigateByUrl('/error/403');
                  break;
                case 401:
                  this.router.navigateByUrl('/login');
                  break;
                case 404:
                  this.router.navigate(['**']);
                  break;
                case 500:
                  this.router.navigateByUrl('/error/500');
              }
            }
          } // console.log(error.error.message);
          console.log(errorResponse);
          this.showLoading = false;
        }
      )
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  showErrorMessage(message: string): void {
    this.loginFail = true;
    this.message = message;
    setTimeout(() => {
      this.loginFail = false;
    }, 3000)
  }
}
