import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user/user';
import { AccountDetails } from '../../models/accountDetails/account-details';
import { NgForm } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { AccountDetailsService } from '../../services/account-details.service';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { SortUp } from 'ng-bootstrap-icons/icons';
import {Router} from '@angular/router';

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.scss']
})
export class RegistrationFormComponent implements OnInit {
  user = new User();
  accountDetails = new AccountDetails();
  accountCreated = false;
  accountFailed = false;
  showLoading = false;
  message = '';

  constructor(private authenticationService: AuthenticationService,
              private accountDetailsService: AccountDetailsService,
              private userService: UserService,
              private router: Router) { }

  ngOnInit(): void {
  }

  processForm(form: NgForm): void {
    this.showLoading = true;
    this.authenticationService.registration(this.user);
    this.accountDetailsService.registration(this.accountDetails);
    this.user.accountDetails = this.accountDetails;
    this.authenticationService.registration(this.user).subscribe((response: HttpResponse<User>) => {
      this.displayMessage(false);
      this.showLoading = false;
      form.reset;
      this.router.navigateByUrl('/login');
    },
      (errorResponse: HttpErrorResponse) => {
        if (errorResponse instanceof HttpErrorResponse) {
          if (errorResponse.error instanceof ErrorEvent) {
            console.error('Error event');
          } else {
            switch (errorResponse.status) {
              case 403:
                this.router.navigateByUrl('/error/403');
                break;
              case 401:
                this.router.navigateByUrl('/login');
                break;
              case 404:
                this.router.navigate(['**']);
                break;
              case 500:
                this.router.navigateByUrl('/error/500');
            }
          }
        } // console.log(error.error.message);
        this.message = errorResponse.error.message;
        // Connection error with backend gives undefined
        if (this.message === undefined) {
          this.message = 'ER IS IETS FOUT GEGAAN PROBEER HET LATER OPNIEUW';
        }
        this.displayMessage(true);
        this.showLoading = false;
      });
  }

  displayMessage(isError: boolean): void {
    this.accountCreated = !isError;
    this.accountFailed = isError;
  }
}
