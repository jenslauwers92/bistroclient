import { Component, OnInit } from '@angular/core';
import { Role } from '../enums/role.enum';
import { AuthenticationService} from '../services/authentication.service';

@Component({
  selector: 'app-account-details',
  templateUrl: './account-details.component.html',
  styleUrls: ['./account-details.component.scss']
})
export class AccountDetailsComponent implements OnInit {
  constructor(public authentication: AuthenticationService) { }
  user = this.authentication.getUserFromLocalCache();
  userDetails = this.user.accountDetails;
  lastLogin = this.user.lastLogin;
  userName = this.user.username;
  isActive = this.user.isActive;
  role = this.user.role;
  firstName = this.userDetails.firstName;
  lastName = this.userDetails.lastName;
  email = this.userDetails.email;
  credits = this.userDetails.credits;

  ngOnInit(): void {
  }
  hasRoleButton(): boolean {
    return this.role === Role.ROLE_OWNER || this.role === Role.ROLE_SUPER_ADMIN;
  }
  isActiveToString(): string {
    if (this.isActive !== true){
      return 'online';
    }else { return 'offline'; }
  }
  roleToString(): string {
    console.log('incomming: ' + this.role);
    console.log('inner: ' + Role.ROLE_OWNER);
    if (this.role === Role.ROLE_SUPER_ADMIN) {
      return 'Super Admin';
    }
    if (this.role === Role.ROLE_OWNER) {
      return 'Uitbater';
    }
    else {return 'Klant'; }
  }
}
