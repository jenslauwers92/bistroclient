import { Component, OnInit } from '@angular/core';
import { RestaurantService } from '../services/restaurant.service';
import {Restaurant} from '../models/restaurant/restaurant';
import {HttpErrorResponse} from '@angular/common/http';
import {AuthenticationService} from '../services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-restaurant-dashboard',
  templateUrl: './restaurant-dashboard.component.html',
  styleUrls: ['./restaurant-dashboard.component.scss']
})
export class RestaurantDashboardComponent implements OnInit {

  loadingRestaurants = false;

  restaurants: Restaurant[] | HttpErrorResponse;

  constructor(private restaurantService: RestaurantService,
              private authenticationService: AuthenticationService,
              private router: Router) { }

  ngOnInit(): void {
    this.getRestaurants();
    console.log(this.restaurants);
  }

  getRestaurants(): void {
    this.loadingRestaurants = true;
    this.restaurantService.getRestaurants()
      .subscribe(restaurants => {
        this.restaurants = restaurants;
        this.loadingRestaurants = false;
      }, (errorResponse: HttpErrorResponse) => {
        console.log(errorResponse.error.message);
        this.loadingRestaurants = false;
      });
  }

  updateRestaurant(restaurant: Restaurant): void {
    console.log(restaurant);
    this.router.navigate(['restaurant/menu/add', restaurant.id]);
  }
}
