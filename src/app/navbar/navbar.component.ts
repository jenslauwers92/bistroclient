import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  isUserLoggedIn = false;

  public isLoggedIn = false;
  public username: string;

  constructor(private authenticationService: AuthenticationService, private router: Router) { }

  ngOnInit(): void {
    // check if user is logged in
    this.authenticationService.username$.subscribe(name => this.username = name);
    this.authenticationService.isLoggedIn();
    this.authenticationService.isUserLoggedIn$.subscribe(loggedIn => this.isLoggedIn = loggedIn);
  }

  logout(): void {
    this.authenticationService.logout();
    this.authenticationService.isUserLoggedIn$.subscribe(loggedIn => this.isLoggedIn = loggedIn);
    this.authenticationService.username$.subscribe(name => this.username = name);
    this.router.navigate(['']);
  }

}
