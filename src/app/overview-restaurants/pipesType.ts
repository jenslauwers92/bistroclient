import { Pipe, PipeTransform } from '@angular/core';
import {Restaurant} from '../models/restaurant/restaurant';

@Pipe({
  name: 'FilterTypePipe',
})
export class FilterTypePipe implements PipeTransform {

  transform(value: Restaurant[], input: string): any {
    if (input) {
      input = input.toLowerCase();
      return value.filter((el: Restaurant) => el.type.toLowerCase().indexOf(input) > -1);
    }
    return value;
  }
}
