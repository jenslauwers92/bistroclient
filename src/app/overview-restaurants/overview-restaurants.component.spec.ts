import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverviewRestaurantsComponent } from './overview-restaurants.component';

describe('OverviewRestaurantsComponent', () => {
  let component: OverviewRestaurantsComponent;
  let fixture: ComponentFixture<OverviewRestaurantsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverviewRestaurantsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverviewRestaurantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
