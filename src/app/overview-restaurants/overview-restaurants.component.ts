import { Component, OnInit } from '@angular/core';
import { Restaurant } from '../models/restaurant/restaurant';
import { RestaurantService } from '../services/restaurant.service';
import {HttpErrorResponse} from '@angular/common/http';
import { Router } from '@angular/router';
import {FeedbackService} from '../services/feedback.service';
import {KitchenType} from '../enums/kitchen-type.enum';

@Component({
  selector: 'app-overview-restaurants',
  templateUrl: './overview-restaurants.component.html',
  styleUrls: ['./overview-restaurants.component.scss']
})
export class OverviewRestaurantsComponent implements OnInit {

  allRestaurants: Restaurant[];
  restaurants: any;
  showLoading = true;
  title = 'Zoek op locatie';
  queryString: any;
  title1 = 'Zoek op naam';
  title2 = 'Zoek op keuken';
  queryNameString: any;
  queryTypeString: any;
  kitchenType: KitchenType;

  constructor(private restaurantService: RestaurantService, private router: Router, private feedbackService: FeedbackService) {}

  ngOnInit(): void {
    this.getAllRestaurants();
  }

  getAllRestaurants(): void {
    this.restaurantService.getAllRestaurants().subscribe(
      resto => {
        this.allRestaurants = resto;
        this.getAverageRating();
        this.showLoading = false;
      }
      , (error: HttpErrorResponse) => {
        if (error instanceof HttpErrorResponse) {
          if (error.error instanceof ErrorEvent) {
            console.error('Error event');
          } else {
            switch (error.status) {
              case 403:
                this.router.navigateByUrl('/error/403');
                break;
              case 401:
                this.router.navigateByUrl('/login');
                break;
              case 500:
                this.router.navigateByUrl('/error/500');
            }
          }
        }
        console.log(error.error.message);
      }
    );
  }

  onSelect(restaurant: Restaurant): void {
    this.router.navigate(['/restaurant', restaurant.id]);
  }

  getAverageRating(): void {
    for (let restaurant of this.allRestaurants) {
      this.feedbackService.getRatingAverage(restaurant.id).subscribe(
        average => restaurant.rating = average
      );
    }
  }
}
