import { Pipe, PipeTransform } from '@angular/core';
import {Restaurant} from '../models/restaurant/restaurant';

@Pipe({
  name: 'FilterPipe',
})
export class FilterPipe implements PipeTransform {

  transform(value: Restaurant[], input: string): any {
    if (input) {
      input = input.toLowerCase();
      return value.filter((el: Restaurant) => el.address.city.toLowerCase().indexOf(input) > -1);
    }
    return value;
  }
}
