/** @author Jens */
import {Menu} from '../restaurant/menu';

export class Dish {
  public id: number;
  public name: string;
  public dishType: number;
  public vegetarian: boolean;
  public vegan: boolean;
  public containsNuts: boolean;
  public price: number;
  public menu: Menu;


  constructor()
  constructor(_vegetarian: boolean, _vegan: boolean, _containsNuts: boolean, _name?: string, _dishType?: number, _price?: number)
  constructor(_vegetarian: boolean, _vegan: boolean, _containsNuts: boolean, _name: string, _dishType: number, _price: number)
  constructor(_vegetarian?: boolean, _vegan?: boolean, _containsNuts?: boolean, _name?: string, _dishType?: number,  _price?: number) {
    this.name = _name;
    this.dishType = _dishType;
    this.vegetarian = _vegetarian;
    this.vegan = _vegan;
    this.containsNuts = _containsNuts;
    this.price = _price;
    }

}
