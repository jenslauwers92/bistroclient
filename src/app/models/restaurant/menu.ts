import {Dish} from '../dish/dish';
import {Drink} from '../drink/drink';

export class Menu {
    public id: number;
    public dishes: Dish[];
    public drinks: Drink[];
}
