import { KitchenType } from 'src/app/enums/kitchen-type.enum';
import { User } from '../user/user';
import { Address } from './address';
import { Menu } from './menu';
import {OpeningHour} from './opening-hour';

export class Restaurant {
  public id: number;
  public name: string;
  public type: KitchenType;
  public rating: number;
  public maxCapacity: number;
  public image: string;
  public parking: boolean;
  public description: string;
  public address: Address;
  public menu: Menu;
  public user: User;
  public openinghours: OpeningHour[];
}
