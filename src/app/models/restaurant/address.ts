export class Address {

    public id: number;
    public street: string;
    public zipcode: string;
    public bus: string;
    public city: string;
    public country: string;

}
