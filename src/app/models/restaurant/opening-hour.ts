import {Day} from '../../enums/day.enum';
import {Time} from '@angular/common';


export class OpeningHour {
  public id: number;
  public day: Day;
  public startHour: Time;
  public endHour: Time;
  public closed: boolean;


  constructor()
  constructor(day: Day, startHour: Time, endHour: Time, closed: boolean, id?: number)
  constructor(day?: Day, startHour?: Time, endHour?: Time, closed?: boolean, id?: number) {
    this.id = id;
    this.day = day;
    this.startHour = startHour;
    this.endHour = endHour;
    this.closed = closed;
  }
}
