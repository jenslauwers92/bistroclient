
/**
 * @author Bregt Pompen
 */
export class Advert {
   id: number;
   link: string;
   text: string;

  // constructor

  constructor()
  constructor(id?: number, link?: string, text?: string) {
    this.id = id;
    this.link = link;
    this.text = text;
  }
}
