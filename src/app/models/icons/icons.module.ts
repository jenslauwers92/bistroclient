import { NgModule } from '@angular/core';

import { BootstrapIconsModule } from 'ng-bootstrap-icons';
import { BarChartLine, Briefcase, Cart4, Clipboard, CreditCard2Back, CalendarCheck, Gift, InfoCircleFill,
  Mouse3, Pen, People, Search , CalendarDay, Mailbox, Circle, ArrowRight, CircleFill, Eye} from 'ng-bootstrap-icons/icons';

const icons = {
  BarChartLine,
  Briefcase,
  Clipboard,
  CreditCard2Back,
  CalendarCheck,
  Cart4,
  Gift,
  InfoCircleFill,
  Mouse3,
  Pen,
  People,
  Search,
  CalendarDay,
  Mailbox,
  Circle,
  CircleFill,
  ArrowRight,
  Eye
};

@NgModule({
  declarations: [],
  imports: [
    BootstrapIconsModule.pick(icons)
  ],
  exports: [
    BootstrapIconsModule
  ]
})
export class IconsModule { }
