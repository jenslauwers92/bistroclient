import {Restaurant} from '../restaurant/restaurant';

export class Feedback {
  public id: number;
  public rating: number;
  public review: string;
  public restaurant: Restaurant;
}
