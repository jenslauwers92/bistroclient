/** @author Jens */
import {Menu} from '../restaurant/menu';

export class Drink {
  public id: number;
  public name: string;
  public type: number;
  public price: number;
  public menu: Menu;
}
