/** @author Jens Lauwers, Kenny Maes, Pelin Yuzbasioglu */
import { AccountDetails } from '../accountDetails/account-details';
import { Role } from 'src/app/enums/role.enum';

export class User {
  public id: number;
  public username: string;
  public isActive: boolean;
  public isNotLocked: boolean;
  public password: string;
  public role: Role;
  public lastLogin: Date;
  public authorities: string[];
  public accountDetails: AccountDetails;


  constructor() {
    this.username = '';
    this.isActive = false;
    this.isNotLocked = false;
  }
}
