export class HttpCustomResponse {
  httpStatusCode: number;
  reason: string;
  message: string;
  httpStatus: string;

  constructor()
  constructor(httpStatusCode?: number, reason?: string, message?: string, httpStatus?: string) {
    this.httpStatusCode = httpStatusCode;
    this.reason = reason;
    this.message = message;
    this.httpStatus = httpStatus;
  }
}
