/** @author Jens Lauwers, Kenny Maes, Pelin Yuzbasioglu */
export class AccountDetails {
  public id: number;
  public firstName: string;
  public lastName: string;
  public email: string;
  public joinDate: Date;
  public reservationId: number;
  public orderId: number;
  public credits: number;

  constructor()
  constructor(id?: number, firstName?: string, lastName?: string, email?: string, credits?: number,
              joinDate?: Date, reservationId?: number, orderId?: number) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.credits = credits;
    this.joinDate = joinDate;
    this.reservationId = reservationId;
    this.orderId = orderId;
  }


  getId(): number {
    return this.id;
  }

  setId(value: number): void {
    this.id = value;
  }

  getFirstName(): string {
    return this.firstName;
  }

  setFirstName(value: string): void {
    this.firstName = value;
  }

  getLastName(): string {
    return this.lastName;
  }

  setLastName(value: string): void {
    this.lastName = value;
  }

  getEmail(): string {
    return this.email;
  }

  setEmail(value: string): void {
    this.email = value;
  }

  getCredits(): number {
    return this.credits;
  }

  setCredits(value: number): void {
    this.credits = value;
  }

  getJoinDate(): Date {
    return this.joinDate;
  }

  setJoinDate(value: Date): void {
    this.joinDate = value;
  }

  getReservationId(): number {
    return this.reservationId;
  }

  setReservationId(value: number): void {
    this.reservationId = value;
  }

  getOrderId(): number {
    return this.orderId;
  }

  setOrderId(value: number): void {
    this.orderId = value;
  }
}
