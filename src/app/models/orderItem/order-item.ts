import {Dish} from '../dish/dish';
import {Drink} from '../drink/drink';

export class OrderItem {

  public id: number;
  public amount: number;
  public price: number;

  public dish: Dish;
  public drink: Drink;
  // public order_id: number;

  constructor()
  constructor(_id?: number, _amount?: number, _price?: number, _dish?: Dish, _drink?: Drink){
    this.id = _id;
    this.amount = _amount;
    this.price = _price;
    this.dish = _dish;
    this.drink = _drink;
    //this.order_id = _order_id;
  }
}
