import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import {UserService} from '../services/user.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AuthenticationService } from '../services/authentication.service';


@Injectable()
export class RoleGuard implements CanActivate {
  private jwtHelper = new JwtHelperService();
  constructor(private router: Router, private auth: AuthenticationService, private userService: UserService) {}

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const expectedRole: string[] = route.data.expectedRoles;
    let hasRole = false;
    if (this.auth.isLoggedIn()) {
      const token = localStorage.getItem('token');
      const tokenInfo = this.jwtHelper.decodeToken(token);
      const authorities: string[] = tokenInfo.Authorities;

      authorities.forEach(auth => {
        expectedRole.forEach(exp => {
          if (auth === exp){
            hasRole = true;
          }
        });
      });

      if (hasRole) {
        return true;
      }else {
        this.router.navigate(['error/403']);
      }
    }else {
      this.router.navigate(['login']);
    }
    return false;
  }
}
