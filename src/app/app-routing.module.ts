import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import {RegistrationFormComponent} from './registration/registration-form/registration-form.component';
import {LoginFormComponent} from './registration/login-form/login-form.component';
import {AdminComponent} from './admin/admin.component';
import {RoleGuard} from './guards/role.guard';
import { AddRestaurantComponent } from './owner/add-restaurant/add-restaurant.component';
import { NoAccessComponent } from './error/no-access/no-access.component';
import { AccountDetailsComponent } from './account-details/account-details.component';
import { PageNotFoundComponent } from './error/page-not-found/page-not-found.component';
import { UnderConstructionComponent} from './error/under-construction/under-construction.component';
import { AddMenuComponent} from './owner/add-menu/add-menu.component';
import { CreditsComponent } from './credits/credits.component';
import {AdvertsComponent} from './adverts/adverts.component';
import {ConfirmDialogComponent} from './confirm-dialog/confirm-dialog.component';
import { FeedbackComponent } from './client/feedback/feedback.component';
import {OrdersComponent} from './orders/orders.component';
import { OverviewRestaurantsComponent } from './overview-restaurants/overview-restaurants.component';
import { RestaurantPageComponent } from './restaurant-page/restaurant-page.component';
import { ReservationsComponent } from './reservations/reservations.component';
import {ServerErrorPageComponent} from './error/server-error-page/server-error-page.component';
import {LoginGuard} from './guards/login.guard';



const routes: Routes = [
  {path: '', component: WelcomeComponent},
  {path: 'registration', component: RegistrationFormComponent},
  {path: 'login', component: LoginFormComponent, canActivate: [LoginGuard]},
  {path: 'admin', component: AdminComponent, canActivate: [RoleGuard], data: { expectedRoles: ['admin'] } },
  {path: 'restaurant/add', component: AddRestaurantComponent, canActivate: [RoleGuard], data: {expectedRoles: ['owner']}},
  {path: 'error/403', component: NoAccessComponent},
  {path: 'account/details', component: AccountDetailsComponent, canActivate: [RoleGuard], data: { expectedRoles: ['user', 'owner'] } },
  {path: 'account/credits', component: CreditsComponent, canActivate: [RoleGuard], data: {expectedRoles: ['user', 'owner']}},
  {path: 'under-construction', component: UnderConstructionComponent},
  {path: 'restaurant/menu/add/:id', component: AddMenuComponent, canActivate: [RoleGuard], data: {expectedRoles: ['owner']}},
  {path: 'account/credits', component: CreditsComponent, canActivate: [RoleGuard], data: {expectedRoles: ['user', 'owner']}},
  {path: 'adverts', component: AdvertsComponent , canActivate: [RoleGuard], data: {expectedRoles: ['owner']}},
  {path: 'confirm', component: ConfirmDialogComponent},
  {path: 'restaurant/feedback/:id', component: FeedbackComponent},
  {path: 'order/:id', component: OrdersComponent},
  {path: 'overview-restaurants', component: OverviewRestaurantsComponent, canActivate: [RoleGuard], data: {expectedRoles: ['user', 'owner']}},
  {path: 'restaurant/:id' , component: RestaurantPageComponent, canActivate: [RoleGuard], data: {expectedRoles: ['user', 'owner']}},
  {path: 'reservaties', component: ReservationsComponent, canActivate: [RoleGuard], data: {expectedRoles: ['user', 'owner']}},
  {path: 'error/500', component: ServerErrorPageComponent},
  {path: '**', component: PageNotFoundComponent}

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
