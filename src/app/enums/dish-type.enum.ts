export enum DishType {
    STARTER = 'Voorgerecht',
    MAIN = 'Hoofdgerecht',
    DESSERT = 'Nagerecht',
    OTHER = 'Andere'
}
