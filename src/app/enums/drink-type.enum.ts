export enum DrinkType {
  SOFT_DRINK= 'Frisdrank',
  BEER = 'Bier',
  WINE = 'Wijn',
  COFFEE = 'Koffie',
  TEA = 'Thee',
  OTHER = 'Andere'

}
