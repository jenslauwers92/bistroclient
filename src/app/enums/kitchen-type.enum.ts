export enum KitchenType {
    MEXICAN = 'Mexicaans',
    AMERICAN_BURGERS = 'Amerikaanse Burgers',
    ITALIAN = 'Italiaans',
    INDIAN = 'Indisch',
    OTHER = 'Andere'
}
