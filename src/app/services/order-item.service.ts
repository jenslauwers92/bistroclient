import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Restaurant} from '../models/restaurant/restaurant';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {OrderItem} from '../models/orderItem/order-item';

@Injectable({
  providedIn: 'root'
})
export class OrderItemService {

  private host = environment.baseUrl;

  constructor(private http: HttpClient ) { }

  public saveOrderItems(orderItems: OrderItem []): Observable<OrderItem[] | HttpErrorResponse> {
    return this.http.post<OrderItem [] | HttpErrorResponse>(`${this.host}/orderitem`, orderItems);

  }
}
