import { Injectable } from '@angular/core';
import { BehaviorSubject, from, Observable, of } from 'rxjs';
import { User } from '../models/user/user';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private host = environment.baseUrl;
  private token: string;
  private jwtHelper = new JwtHelperService();
  private loggedInUser: string;

  // Observables
  private loggedInUsernameSource = new BehaviorSubject<string>('');
  public username$ = this.loggedInUsernameSource.asObservable();
  private isUserLoggedInSource = new BehaviorSubject<boolean>(false);
  public isUserLoggedIn$ = this.isUserLoggedInSource.asObservable();

  constructor(private http: HttpClient) {
  }

  public login(user: User): Observable<HttpResponse<any> | HttpErrorResponse>{
    return this.http.post<HttpResponse<any> | HttpErrorResponse>(`${this.host}/user/login`, user, {observe: 'response'});
  }

  public registration(user: User): Observable<HttpResponse<any> | HttpErrorResponse>{
    return this.http.post<HttpResponse<any> | HttpErrorResponse>(`${this.host}/user/register`, user);
  }

  public logout(): void {
    this.token = null;
    this.loggedInUser = null;
    localStorage.removeItem('user');
    localStorage.removeItem('users');
    localStorage.removeItem('token');
    this.loggedInUsernameSource.next('');
    this.isUserLoggedInSource.next(false);
}

  public saveToken(token: string): void {
    this.token = token;
    localStorage.setItem('token', token);
  }

  public addUserToLocalCache(user: User): void {

    // set user to json string
    localStorage.setItem('user', JSON.stringify(user));
  }

  public getUserFromLocalCache(): User {
    // set user from string to User object
    return JSON.parse(localStorage.getItem('user'));
  }

  public loadToken(): void {
    this.token = localStorage.getItem('token');
  }

  public getToken(): string {
    return this.token;
  }

  public setUsername(username: string) {
    this.loggedInUsernameSource.next(username);
  }

  public isLoggedIn(): boolean {
    this.loadToken();
    
    if(this.token != null && this.token !== '') {
      if(this.jwtHelper.decodeToken(this.token).sub != null || '') {
        if(!this.jwtHelper.isTokenExpired(this.token)) {
          this.loggedInUser = this.jwtHelper.decodeToken(this.token).sub;
          this.isUserLoggedInSource.next(true);
          this.loggedInUsernameSource.next(this.getUserFromLocalCache().username);
          return true;
        }
      }
    } else {
      this.logout();
      return false;
    }
  }
}
