import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';
import { Feedback } from '../models/feedback/feedback';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  private host = environment.baseUrl;

  constructor(private http: HttpClient) { }

  loadFeedbacks(restaurantId: number): Observable<Feedback[]> {
    return this.http.get<Feedback[]>(`${this.host}/restaurants/${restaurantId}/feedbacks`);
  }

  getRatingAverage(restaurantId: number): Observable<number> {
    return this.http.get<number>(`${this.host}/restaurants/${restaurantId}/feedbacks/average`);
  }

  saveFeedback(feedback: Feedback, restaurantId: number): Observable<Feedback | HttpErrorResponse> {
    return this.http.post<Feedback | HttpErrorResponse>(`${this.host}/restaurants/${restaurantId}/feedbacks`, feedback);
  }

  removeFeedback(feedbackId: number, restaurantId: number): void {
    this.http.delete(`${this.host}restaurants/${restaurantId}/feedback/${feedbackId}`).subscribe(response => {
      console.log(response);
    });
  }

  updateFeedback(feedback: Feedback, restaurantId: number): void {
    this.http.put(`${this.host}/restaurants/${restaurantId}/feedback`, feedback).subscribe(response => {
      console.log(response);
    });
  }
}
