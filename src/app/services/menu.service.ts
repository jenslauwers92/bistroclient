import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from 'src/environments/environment';
import {Dish} from '../models/dish/dish';
import {Drink} from '../models/drink/drink';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  private host = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  loadDishes(menuId: number): Observable<Dish[] | HttpErrorResponse> {
    return this.http.get<Dish[] | HttpErrorResponse>(`${this.host}/menu/${menuId}/dish`);
  }

  loadDishesByType(menuId: number): Observable<Dish[] | HttpErrorResponse> {
    return this.http.get<Dish[] | HttpErrorResponse>(`${this.host}/menu/${menuId}/dish/type`);
  }

  saveDish(dish: Dish, menuId: number): Observable<Dish | HttpErrorResponse> {
    return this.http.post<Dish | HttpErrorResponse>(`${this.host}/menu/${menuId}/dish`, dish);
  }

  removeDish(dishId: number, menuId: number): Observable<any | HttpErrorResponse> {
    return this.http.delete<any | HttpErrorResponse>(`${this.host}/menu/${menuId}/dish/${dishId}`);
  }

  loadDrinks(menuId: number): Observable<Drink[] | HttpErrorResponse> {
    return this.http.get<Drink[] | HttpErrorResponse>(`${this.host}/menu/${menuId}/drink`);
  }

  loadDrinksByType(menuId: number): Observable<Drink[] | HttpErrorResponse> {
    return this.http.get<Drink[] | HttpErrorResponse>(`${this.host}/menu/${menuId}/drink/type`);
  }

  saveDrink(drink: Drink, menuId: number): Observable<Drink | HttpErrorResponse> {
    return this.http.post<Drink | HttpErrorResponse>(`${this.host}/menu/${menuId}/drink`, drink);
  }

  removeDrink(drinkId: number, menuId: number): Observable<any | HttpErrorResponse> {
    return this.http.delete<any | HttpErrorResponse>(`${this.host}/menu/${menuId}/drink/${drinkId}`);
  }

  updateDrink(drink: Drink): Observable<Drink | HttpErrorResponse> {
    return this.http.put<Drink | HttpErrorResponse>(`${this.host}/menu/0/drink`, drink);
  }

  updateDish(dish: Dish): Observable<Dish | HttpErrorResponse> {
    return this.http.put<Dish | HttpErrorResponse>(`${this.host}/menu/0/dish`, dish);
  }
}
