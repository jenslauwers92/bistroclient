import { Injectable } from '@angular/core';
import { AccountDetails } from '../models/accountDetails/account-details';
import { Observable, of } from 'rxjs';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import {Dish} from '../models/dish/dish';

@Injectable({
  providedIn: 'root'
})
export class AccountDetailsService {
  private host = environment.baseUrl;
  private token: string;
  private loggedInUsername: string;
  constructor(private http: HttpClient) { }

  public getAccountDetails(): Observable<AccountDetails | HttpErrorResponse> {
    return this.http.get<AccountDetails>(`${this.host}/accountDetails`);
  }

  public getCurrentAccountDetail(username: string): Observable<AccountDetails> {
    return this.http.get<AccountDetails>(`${this.host}/accountDetails/username/${username}`);
  }

  public updateCredits(userName: string, addition: number): Observable<AccountDetails | HttpErrorResponse>{
    const params = new HttpParams().set('addition', String(addition));
    return this.http.put<AccountDetails | HttpErrorResponse>(`${this.host}/accountDetails/${userName}/update`, null, { params } );
  }

  public login(accountDetails: AccountDetails): Observable<HttpResponse<any> | HttpErrorResponse>{
    return this.http.post(`${this.host}/accountDetails/login`, accountDetails, {observe: 'response'});
  }

  public registration(accountDetails: AccountDetails): Observable<HttpResponse<any> | HttpErrorResponse>{
    const HEADERS = new HttpHeaders({'Access-Control-Allow-Origin': 'Http://localhost:4200/'});
    return this.http.post<HttpResponse<any> | HttpErrorResponse>(`${this.host}/accountDetails/register`,
      accountDetails);
  }

  public logout(): void {
    this.token = null;
    this.loggedInUsername = null;
    localStorage.removeItem('accountDetails');
    localStorage.removeItem('token');
  }

  public saveToken(token: string): void {
    this.token = token;
    localStorage.setItem('token', token);
  }

  public addUserToLocalCache(accountDetails: AccountDetails): void {
    localStorage.setItem('user', JSON.stringify(accountDetails));
  }
}
