import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Advert} from '../models/ads/advert';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdvertService {

  constructor(private http: HttpClient) { }
  private host = environment.baseUrl;

  public saveAdvert(advert: Advert): Observable<Advert | HttpErrorResponse>{
    return this.http.post<Advert | HttpErrorResponse>(`${this.host}/adverts`, advert );
  }

  public getAdverts(): Observable<Advert[] | HttpErrorResponse> {
    return this.http.get<Advert[]>(`${this.host}/adverts`);
  }
}
