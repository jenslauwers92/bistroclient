import { Injectable } from '@angular/core';
import { from, Observable, of } from 'rxjs';
import { User } from '../models/user/user';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private host = environment.baseUrl;

  constructor(private http: HttpClient) {}

  public getUsers(): Observable<User[] | HttpErrorResponse> {
    return this.http.get<User[]>(`${this.host}/user/list`);
  }

  public addUser(formData: FormData): Observable<User | HttpErrorResponse> {
    return this.http.post<User>(`${this.host}/user/add`, formData);
  }

  public updateUser(formData: FormData): Observable<User | HttpErrorResponse> {
    return this.http.post<User>(`${this.host}/user/update`, formData);
  }

  public resetPassword(email: String): Observable<any | HttpErrorResponse> {
    return this.http.get(`${this.host}/user/reset-password/${email}`);
  }

  public deleteUser(userId: number): Observable<any | HttpErrorResponse> {
    return this.http.delete<User>(`${this.host}/user/delete/${userId}`);
  }

  public addUsersToLocalCache(users: User[]): void {
    localStorage.setItem('users', JSON.stringify(users));
  }

  public getUsersToLocalCache(): User[] {
    if(localStorage.getItem('users')) {
      return JSON.parse(localStorage.getItem('users'));
    }
  }

  public createUserFormData(loggedInUsername: string, user: User): FormData {
    const formData = new FormData();
    formData.append('currentUsername', loggedInUsername);
    formData.append('firstName', user.accountDetails.firstName);
    formData.append('lastName', user.accountDetails.lastName);
    formData.append('username', user.username);
    formData.append('email', user.accountDetails.email);
    formData.append('role', JSON.stringify(user.role));
    formData.append('credits', JSON.stringify(user.accountDetails.credits));
    formData.append('isActive', JSON.stringify(user.isActive));
    formData.append('isNotLocked', JSON.stringify(loggedInUsername));
    return formData;
  }

}
