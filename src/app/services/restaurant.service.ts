import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Restaurant } from '../models/restaurant/restaurant';
import {OpeningHour} from '../models/restaurant/opening-hour';
import {Feedback} from '../models/feedback/feedback';


@Injectable({
  providedIn: 'root'
})
export class RestaurantService {
  private host = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  public saveRestaurant(restaurant: Restaurant): Observable<Restaurant | HttpErrorResponse> {
    return this.http.post<Restaurant | HttpErrorResponse>(`${this.host}/owner/restaurants`, restaurant);
  }

  public getRestaurants(): Observable<Restaurant[] | HttpErrorResponse> {
    return this.http.get<Restaurant[]>(`${this.host}/owner/restaurants`);
  }

  public getOwnerRestaurant(id: number): Observable<Restaurant | HttpErrorResponse> {
    return this.http.get<Restaurant | HttpErrorResponse>(`${this.host}/owner/restaurants/${id}`);
  }

  public getRestaurant(id: number): Observable<Restaurant | HttpErrorResponse> {
    return this.http.get<Restaurant | HttpErrorResponse>(`${this.host}/restaurants/${id}`);
  }

  loadHours(id: number): Observable<OpeningHour[] | HttpErrorResponse> {
    return this.http.get<OpeningHour[] | HttpErrorResponse>(`${this.host}/restaurants/${id}/opening-hours`);
  }

  updateHour(openingHour: OpeningHour): Observable<OpeningHour | HttpErrorResponse> {
    return this.http.put<OpeningHour | HttpErrorResponse>(`${this.host}/restaurants/update/opening-hour`, openingHour);
  }

  loadRatings(restaurantId: number): Observable<Feedback[] | HttpErrorResponse> {
    return this.http.get<Feedback[] | HttpErrorResponse>(`${this.host}/restaurants/${restaurantId}/feedbacks`);
  }

  public getRestaurantById(id: number): Observable<Restaurant> {
    return this.http.get<Restaurant>(`${this.host}/restaurants/${id}`);
  }

  getAllRestaurants(): Observable<any | HttpErrorResponse> {
    return this.http.get<any | HttpErrorResponse>(`${this.host}/restaurants`);
  }

  public updateRestaurant(restaurant: Restaurant): Observable<Restaurant | HttpErrorResponse> {
    return this.http.put<Restaurant | HttpErrorResponse>(`${this.host}/owner/restaurants`, restaurant);
  }

}
