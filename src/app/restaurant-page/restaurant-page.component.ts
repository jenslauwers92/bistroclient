import { Component, OnInit} from '@angular/core';
import { Restaurant } from '../models/restaurant/restaurant';
import { RestaurantService } from '../services/restaurant.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@Angular/router';
import {FeedbackService} from '../services/feedback.service';
import {Feedback} from '../models/feedback/feedback';
import { from } from 'rxjs';

@Component({
  selector: 'app-restaurant-page',
  templateUrl: './restaurant-page.component.html',
  styleUrls: ['./restaurant-page.component.scss']
})
export class RestaurantPageComponent implements OnInit {

  restaurant: Restaurant;
  restaurantId: number;
  showLoading = true;
  feedbacks: Feedback[] = [];
  averageRating = 0;

  constructor(private restaurantService: RestaurantService,
              private feedbackService: FeedbackService,
              private route: ActivatedRoute,
              private router: Router) {}

  ngOnInit(): void {
    this.restaurantService.getRestaurant(Number(this.route.snapshot.paramMap.get('id')))
      .subscribe((restaurant: Restaurant) => {
        this.restaurant = restaurant;
        this.showLoading = false;
        this.getFeedbacks();
        this.getAverageRating();
      });
    // const id = parseInt(this.route.snapshot.paramMap.get('id'));
    // this.restaurantId = id;
    // this.getRestaurant();
  }

  getRestaurant(): void {
    this.restaurantService.getRestaurant(this.restaurant.id).subscribe(
      (restaurant: Restaurant) => {
        this.restaurant = restaurant;
        console.log(restaurant);
        this.showLoading = false;
      },
      (errorResponse: HttpErrorResponse) => {
        if (errorResponse instanceof HttpErrorResponse) {
          if (errorResponse.error instanceof ErrorEvent) {
            console.error('Error event');
          } else {
            switch (errorResponse.status) {
              case 403:
                this.router.navigateByUrl('/error/403');
                break;
              case 401:
                this.router.navigateByUrl('/login');
                break;
              case 404:
                this.router.navigate(['**']);
                break;
              case 500:
                this.router.navigateByUrl('/error/500');
            }
          }
        }
        console.log(errorResponse.error.message);
      }
    );
  }

  getFeedbacks(): void {
    this.feedbackService.loadFeedbacks(this.restaurant.id).subscribe( (feedbacks: any) => {
      this.feedbacks = feedbacks.feedbacks;
    });
  }

  getAverageRating(): void {
    this.feedbackService.getRatingAverage(this.restaurant.id).subscribe(
      average => this.averageRating = average
    );
  }

  onSelect(restaurant: Restaurant): void {
    this.router.navigate(['/restaurant/feedback', restaurant.id]);
  }

  goToMenu(restaurant: Restaurant): void {
    this.router.navigate(['/order/', restaurant.id]);
  }
}
