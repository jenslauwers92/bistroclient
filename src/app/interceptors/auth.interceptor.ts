import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthenticationService } from '../services/authentication.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authenticationService: AuthenticationService) {}

  intercept(httpRequest: HttpRequest<unknown>, httpHandler: HttpHandler): Observable<HttpEvent<unknown>> {
    if(httpRequest.url.includes(`${environment.baseUrl}/user/login`)) {
      return httpHandler.handle(httpRequest);
    }
    if(httpRequest.url.includes(`${environment.baseUrl}/user/register`)) {
      return httpHandler.handle(httpRequest);
    }
    if(httpRequest.url.includes(`${environment.baseUrl}/user/resetPassword`)) {
      return httpHandler.handle(httpRequest);
    }
    if(httpRequest.url.includes(`${environment.baseUrl}/adverts`)) {
      return httpHandler.handle(httpRequest);
    }

    this.authenticationService.loadToken();
    const token = this.authenticationService.getToken();
    const request = httpRequest.clone({setHeaders: {Authorization: `Bearer ${token}`}});
    return httpHandler.handle(request);
  }
}
