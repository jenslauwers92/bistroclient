import { Component, OnInit } from '@angular/core';
import {FormControl, NgForm, Validators} from '@angular/forms';
import {AuthenticationService} from '../services/authentication.service';
import {AdvertService} from '../services/advert.service';
import {Advert} from '../models/ads/advert';
import {HttpErrorResponse} from '@angular/common/http';
import {AccountDetails} from '../models/accountDetails/account-details';
import {AccountDetailsService} from '../services/account-details.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-adverts',
  templateUrl: './adverts.component.html',
  styleUrls: ['./adverts.component.scss']
})
export class AdvertsComponent implements OnInit {

  newAdvert = new Advert();
  user = this.authentication.getUserFromLocalCache();
  credits: number;
  userName = this.user.username;
  refreshedUserDetails: AccountDetails;
  adverts: Advert[] | HttpErrorResponse;
  showLoading = false;

  constructor(private authentication: AuthenticationService,
              private advertService: AdvertService,
              private accountDetails: AccountDetailsService,
              private router: Router) {
  }

  ngOnInit(): void {
     this.getAds();
     this.getUserDetails();
     this.checkMoney();
  }

  getAds(): void {
    this.advertService.getAdverts().subscribe(adverts => { // subscribe voert methode uit
      this.adverts = adverts;
      console.log(this.adverts);
    });
  }

  checkMoney(): boolean {
  if (this.credits < 5){
  return true;
  }
  return false;
}

  processForm(buyForm: NgForm): void {
    console.log(this.newAdvert);
    this.showLoading = true;
    if (this.credits >= 5){
    this.advertService.saveAdvert(this.newAdvert).subscribe(
      (advert: Advert) => {
        console.log(advert);
        this.showLoading = false;
        buyForm.reset();
        this.router.navigateByUrl('');
      }, (errorResponse: HttpErrorResponse) => {
        if (errorResponse instanceof HttpErrorResponse) {
          if (errorResponse.error instanceof ErrorEvent) {
            console.error('Error event');
          } else {
            switch (errorResponse.status) {
              case 403:
                this.router.navigateByUrl('/error/403');
                break;
              case 401:
                this.router.navigateByUrl('/login');
                break;
              case 404:
                this.router.navigate(['**']);
                break;
              case 500:
                this.router.navigateByUrl('/error/500');
            }
          }
        }
        console.log(errorResponse);
        this.showLoading = false;
      }
    );
    this.accountDetails.updateCredits(this.userName, -5).subscribe(
      (accountDetails: AccountDetails) => {
        console.log(accountDetails);
      }, (errorResponse: HttpErrorResponse) => {
        if (errorResponse instanceof HttpErrorResponse) {
          if (errorResponse.error instanceof ErrorEvent) {
            console.error('Error event');
          } else {
            switch (errorResponse.status) {
              case 403:
                this.router.navigateByUrl('/error/403');
                break;
              case 401:
                this.router.navigateByUrl('/login');
                break;
              case 404:
                this.router.navigate(['**']);
                break;
              case 500:
                this.router.navigateByUrl('/error/500');
            }
          }
          console.log(errorResponse);
        }
      }
        );
    } else {
      this.checkMoney();
    }
  }

  getUserDetails(): void {
    this.accountDetails.getCurrentAccountDetail(this.userName).subscribe(refreshedUserDetails => { // subscribe voert methode uit
      this.credits = refreshedUserDetails.credits;
      console.log(this.refreshedUserDetails);
      console.log(this.credits);
    }
    );
  }
}
