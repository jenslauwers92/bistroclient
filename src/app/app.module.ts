import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { RegistrationFormComponent } from './registration/registration-form/registration-form.component';
import {FormsModule} from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginFormComponent } from './registration/login-form/login-form.component';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { RoleGuard} from './guards/role.guard';
import { AdminComponent } from './admin/admin.component';
import {IconsModule} from './models/icons/icons.module';
import { AddRestaurantComponent } from './owner/add-restaurant/add-restaurant.component';
import { NoAccessComponent } from './error/no-access/no-access.component';
import { AccountDetailsComponent } from './account-details/account-details.component';
import { EmailValidatorDirective } from './validators/email-validator.directive';
import { PageNotFoundComponent } from './error/page-not-found/page-not-found.component';
import { UnderConstructionComponent } from './error/under-construction/under-construction.component';
import { RestaurantDashboardComponent } from './restaurant-dashboard/restaurant-dashboard.component';
import { AddMenuComponent } from './owner/add-menu/add-menu.component';
import { CreditsComponent } from './credits/credits.component';
import { AdvertsComponent } from './adverts/adverts.component';
import { FeedbackComponent } from './client/feedback/feedback.component';
import { OrdersComponent } from './orders/orders.component';
import { OverviewRestaurantsComponent } from './overview-restaurants/overview-restaurants.component';
import { RestaurantPageComponent } from './restaurant-page/restaurant-page.component';
import { UpdateRestaurantComponent } from './owner/update-restaurant/update-restaurant.component';
import { AddDishesComponent } from './owner/add-dishes/add-dishes.component';
import { AddDrinksComponent } from './owner/add-drinks/add-drinks.component';
import {FilterPipe} from './overview-restaurants/pipes';
import {FilterNamePipe} from './overview-restaurants/pipesName';
import {FilterTypePipe} from './overview-restaurants/pipesType';
import { ReservationsComponent } from './reservations/reservations.component';
import { ServerErrorPageComponent } from './error/server-error-page/server-error-page.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    WelcomeComponent,
    RegistrationFormComponent,
    LoginFormComponent,
    AdminComponent,
    AddRestaurantComponent,
    NoAccessComponent,
    AccountDetailsComponent,
    EmailValidatorDirective,
    PageNotFoundComponent,
    CreditsComponent,
    UnderConstructionComponent,
    RestaurantDashboardComponent,
    AddMenuComponent,
    CreditsComponent,
    AdvertsComponent,
    FeedbackComponent,
    OrdersComponent,
    OverviewRestaurantsComponent,
    RestaurantPageComponent,
    UpdateRestaurantComponent,
    AddDishesComponent,
    AddDrinksComponent,
    FilterPipe,
    FilterNamePipe,
    FilterTypePipe,
    ReservationsComponent,
    ServerErrorPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    IconsModule
  ],
  providers: [RoleGuard, { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }],
  bootstrap: [AppComponent]
})


export class AppModule { }
