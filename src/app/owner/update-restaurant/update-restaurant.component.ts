import {HttpErrorResponse} from '@angular/common/http';
import {AfterViewInit, Component, DoCheck, Input, OnInit, SimpleChanges} from '@angular/core';
import {Day} from 'src/app/enums/day.enum';
import {KitchenType} from 'src/app/enums/kitchen-type.enum';
import {Address} from 'src/app/models/restaurant/address';
import {OpeningHour} from 'src/app/models/restaurant/opening-hour';
import {RestaurantService} from 'src/app/services/restaurant.service';
import {Restaurant} from '../../models/restaurant/restaurant';
import {Router} from '@angular/router';
import { OnChanges } from '@angular/core';
import {trigger} from '@angular/animations';

@Component({
  selector: 'app-update-restaurant',
  templateUrl: './update-restaurant.component.html',
  styleUrls: ['./update-restaurant.component.scss']
})
export class UpdateRestaurantComponent implements OnInit, AfterViewInit, OnChanges {

  // inputs
  @Input() restaurant: Restaurant;
  @Input() address: Address;

  // checks
  public isImageSelected = false;
  public showRestoLoading = false;
  public isOpeningHourListLoading = false;
  public parking: boolean;
  public hoursAreValid = false;

  // enums
  public keys = Object.keys;
  public day = Day;
  public kitchenTypes = KitchenType;

  // fields
  public openingHours: OpeningHour[] = [];
  public editOpeningHourId: number;
  public editOpeningHour: OpeningHour;
  public openingHour: OpeningHour;
  private previousSelected;


  constructor(private restaurantService: RestaurantService, private router: Router) {
    this.editOpeningHour = new OpeningHour();
    this.openingHour = new OpeningHour();
  }

  ngOnInit(): void {

  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.restaurant) {
      this.loadHours();
    }
  }

  ngAfterViewInit(): void {
    const selectedImage = document.getElementById(this.restaurant.image);
    console.log(selectedImage);
    selectedImage.classList.add('show');
    this.previousSelected = selectedImage;
  }

  loadHours(): void {
    this.isImageSelected = this.restaurant.image != null;
    this.isOpeningHourListLoading = true;
    this.restaurantService.loadHours(this.restaurant.id).subscribe((data: OpeningHour[]) => {
        this.openingHours = data;

        this.isOpeningHourListLoading = false;
      },
      (errorResponse: HttpErrorResponse) => {
        if (errorResponse instanceof HttpErrorResponse) {
          if (errorResponse.error instanceof ErrorEvent) {
            console.error('Error event');
          } else {
            switch (errorResponse.status) {
              case 403:
                this.router.navigateByUrl('/error/403');
                break;
              case 401:
                this.router.navigateByUrl('/login');
                break;
              case 404:
                this.router.navigate(['**']);
                break;
              case 500:
                this.router.navigateByUrl('/error/500');
            }
          }
        }
        console.log(errorResponse.error.message);
        this.isOpeningHourListLoading = false;
      });
  }

  saveUpdateHour(): void {
    this.restaurantService.updateHour(this.editOpeningHour).subscribe(data => {
      this.loadHours();
    }, (errorResponse: HttpErrorResponse) => {
      if (errorResponse instanceof HttpErrorResponse) {
        if (errorResponse.error instanceof ErrorEvent) {
          console.error('Error event');
        } else {
          switch (errorResponse.status) {
            case 403:
              this.router.navigateByUrl('/error/403');
              break;
            case 401:
              this.router.navigateByUrl('/login');
              break;
            case 404:
              this.router.navigate(['**']);
              break;
            case 500:
              this.router.navigateByUrl('/error/500');
          }
        }
      }
      console.log(errorResponse.error.message);
    });
  }

  onHourChange(): void {
    // TODO: check time
  }

  updateForm(): void {
    console.log(this.restaurant);
  }

  onUpdateHours(openingHour: OpeningHour): void {
    this.hoursAreValid = true;
    this.editOpeningHour.id = openingHour.id;
    this.editOpeningHour.day = openingHour.day;
    this.editOpeningHour.startHour = openingHour.startHour;
    this.editOpeningHour.endHour = openingHour.endHour;
    this.editOpeningHour.closed = openingHour.closed;
  }

  checkImage(event): void {
    const classes = event.target.classList;
    // check or uncheck the image
    if (classes.contains('show')) {
      classes.remove('show');
      this.restaurant.image = '';
      this.isImageSelected = false;
    } else {
      // check if other image is checked when new image is checked previous will be remove the check
      if (this.previousSelected != null) {
        this.previousSelected.classList.remove('show');
        this.isImageSelected = false;
      }
      this.previousSelected = event.target;
      classes.add('show');
      this.isImageSelected = true;
      this.restaurant.image = event.target.id;
    }
  }

  getDay(day: string): string {
    switch (day) {
      case 'MONDAY':
        return 'Maandag';
      case 'TUESDAY':
        return 'dinsdag';
      case 'WEDNESDAY':
        return 'woensdag';
      case 'THURSDAY':
        return 'Donderdag';
      case 'FRIDAY':
        return 'Vrijdag';
      case 'SATURDAY':
        return 'Zaterdag';
      case 'SUNDAY':
        return 'Zondag';
    }
  }

  arrayOne(n: number): any[] {
    return Array(n);
  }

  updateRestaurant() {
    const triggerBtn = document.getElementById("updateModalTrigger") as HTMLElement;
    const updatedRestaurant: Restaurant = new Restaurant();
    const updatedAddress: Address = new Address();

    updatedAddress.id = this.restaurant.address.id;
    updatedAddress.street = this.restaurant.address.street;
    updatedAddress.bus = this.restaurant.address.bus;
    updatedAddress.city = this.restaurant.address.city;
    updatedAddress.country = this.restaurant.address.country;
    updatedAddress.zipcode = this.restaurant.address.zipcode;

    updatedRestaurant.id = this.restaurant.id;
    updatedRestaurant.address = updatedAddress;
    updatedRestaurant.name = this.restaurant.name;
    updatedRestaurant.type = this.restaurant.type;
    updatedRestaurant.maxCapacity = this.restaurant.maxCapacity;
    updatedRestaurant.parking = this.restaurant.parking;
    updatedRestaurant.image = this.restaurant.image;
    updatedRestaurant.description = this.restaurant.description;

    this.showRestoLoading = true;
    this.restaurantService.updateRestaurant(updatedRestaurant).subscribe((data: Restaurant) => {
      this.restaurant = data;
      this.showRestoLoading = false;

      triggerBtn.click();
    }, (errorResponse: HttpErrorResponse) => {
      this.showRestoLoading = false;
      if (errorResponse instanceof HttpErrorResponse) {
        if (errorResponse.error instanceof ErrorEvent) {
          console.error('Error event');
        } else {
          switch (errorResponse.status) {
            case 403:
              this.router.navigateByUrl('/error/403');
              break;
            case 401:
              this.router.navigateByUrl('/login');
              break;
            case 404:
              this.router.navigate(['**']);
              break;
            case 500:
              this.router.navigateByUrl('/error/500');
          }
        }
      }
    });
  }
}
