import {Component, DoCheck, Input, OnChanges, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Drink} from '../../models/drink/drink';
import {HttpErrorResponse} from '@angular/common/http';
import {MenuService} from '../../services/menu.service';
import {Restaurant} from '../../models/restaurant/restaurant';
import {Menu} from '../../models/restaurant/menu';
import {DrinkType} from '../../enums/drink-type.enum';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-drinks',
  templateUrl: './add-drinks.component.html',
  styleUrls: ['./add-drinks.component.scss']
})
export class AddDrinksComponent implements OnInit, OnChanges {

  // input from parent
  @Input()restaurant: Restaurant;
  @Input()menu: Menu;

  public isListLoading = false;
  public isDrinkListLoading = false;
  public showLoading = false;
  public showIsRemoved = false;

  public drinks: Drink[] = [];
  public drink: Drink = new Drink();
  public editDrink: Drink = new Drink();
  public editDrinkId: number;
  public drinkToRemove: Drink = new Drink();

  // lists
  public keys = Object.keys;
  public drinkTypes = DrinkType;
  message = 'Item is verwijderd';
  public showError: boolean;

  constructor(private menuService: MenuService, private router: Router) { }

  ngOnInit(): void {

  }

  ngOnChanges(): void {
    if (this.menu) {
      this.loadDrinks();
    }
  }

  addDrink(form: NgForm): void {
    console.log(this.drink);
    this.showLoading = true;
    this.menuService.saveDrink(this.drink, this.menu.id).subscribe((drink: Drink) => {
        this.showLoading = false;
        this.loadDrinks();
      },
      (errorResponse: HttpErrorResponse) => {
          if (errorResponse.error instanceof ErrorEvent) {
            console.error('Error event');
          } else {
            switch (errorResponse.status) {
              case 403:
                this.router.navigateByUrl('/error/403');
                break;
              case 401:
                this.router.navigateByUrl('/login');
                break;
              case 404:
                this.router.navigate(['**']);
                break;
              case 500:
                this.router.navigateByUrl('/error/500');
            }
        }
          console.log(errorResponse.error.message);
          this.showLoading = false;
      });
    form.reset();
    this.drink = new Drink();
  }

  onUpdateDrink(drink: Drink): void{
    this.editDrink.id = drink.id;
    this.editDrink.name = drink.name;
    this.editDrink.price = drink.price;
    this.editDrink.type = drink.type;
    this.editDrinkId = drink.id;
    this.editDrink.menu = this.menu;
  }

  saveUpdateDrink(): void{
    console.log(this.editDrink);
    this.menuService.updateDrink(this.editDrink).subscribe((data: Drink) => {
      this.loadDrinks();
    },
      (errorResponse: HttpErrorResponse) => {
        console.log(errorResponse.error.message);
      });
  }

  removeDrink(): void {
    this.menuService.removeDrink(this.drinkToRemove.id, this.menu.id).subscribe(data => {
      this.showInfoMessage(this.drinkToRemove.name + ' is verwijderd van het menu');
      this.loadDrinks();
    }, (errorResponse: HttpErrorResponse) => {
        if (errorResponse.status === 500) {
          this.showErrorMessage('Er is iets fout gegaan bij het verwijderen probeer later opnieuw');
        }
      });
  }

  loadDrinks(): void {
      this.isDrinkListLoading = true;
      this.menuService.loadDrinks(this.menu.id).subscribe( (data: Drink[]) => {
          data.forEach(d => {
            console.log(d.name + ', ' + d.id);
          });
          this.drinks = data;
          this.isDrinkListLoading = false;
        },
        (errorResponse: HttpErrorResponse) => {
        if (errorResponse instanceof HttpErrorResponse) {
            if (errorResponse.error instanceof ErrorEvent) {
              console.error('Error event');
            } else {
              switch (errorResponse.status) {
                case 403:
                  this.router.navigateByUrl('/error/403');
                  break;
                case 401:
                  this.router.navigateByUrl('/login');
                  break;
                case 404:
                  this.router.navigate(['**']);
                  break;
                case 500:
                  this.router.navigateByUrl('/error/500');
              }
            }
          }
        console.log(errorResponse.error.message);
        this.isDrinkListLoading = false;
        });
    }

  setToBeRemoved(item: Drink): void {
    console.log(item.name);
    this.drinkToRemove = item;
  }

  private showInfoMessage(message: string): void {
    this.showIsRemoved = true;
    this.message = message;
    // show message for 3 seconds
    setTimeout(() => {
      this.showIsRemoved = false;
    }, 3000);
  }

  private showErrorMessage(message: string): void {
    this.showError = true;
    this.message = message;
    // show message for 3 seconds
    setTimeout(() => {
      this.showError = false;
    }, 3000);
  }
}
