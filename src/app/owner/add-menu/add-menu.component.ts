import { Component, OnInit } from '@angular/core';
import {RestaurantService} from '../../services/restaurant.service';
import {Restaurant} from '../../models/restaurant/restaurant';
import {Menu} from '../../models/restaurant/menu';
import { MenuService } from '../../services/menu.service';
import { HttpErrorResponse } from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {Address} from '../../models/restaurant/address';

@Component({
  selector: 'app-add-menu',
  templateUrl: './add-menu.component.html',
  styleUrls: ['./add-menu.component.scss']
})
export class AddMenuComponent implements OnInit {

  public restaurant: Restaurant;
  public address: Address;
  public menu: Menu;

  constructor(private restaurantService: RestaurantService, private menuService: MenuService,
              private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {

    // Get restaurant address and menu for passing to child components
    this.restaurantService.getOwnerRestaurant(
      Number(this.activatedRoute.snapshot.paramMap.get('id'))).subscribe((restaurant: Restaurant) => {
      this.restaurant = restaurant;
      this.address = restaurant.address;
      this.menu = this.restaurant.menu;
      }, (errorResponse: HttpErrorResponse) => {
      if (errorResponse instanceof HttpErrorResponse) {
        if (errorResponse.error instanceof ErrorEvent) {
          console.error('Error event');
        } else {
          switch (errorResponse.status) {
            case 403:
              this.router.navigateByUrl('/error/403');
              break;
            case 401:
              this.router.navigateByUrl('/login');
              break;
            case 404:
              this.router.navigate(['**']);
              break;
            case 500:
              this.router.navigateByUrl('/error/500');
          }
        }
      }
      console.log(errorResponse.error.message);
    });
  }
}
