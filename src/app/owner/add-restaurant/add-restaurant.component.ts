import { Component, OnInit } from '@angular/core';
import { Address } from 'src/app/models/restaurant/address';
import { Restaurant } from 'src/app/models/restaurant/restaurant';
import { KitchenType } from 'src/app/enums/kitchen-type.enum';
import { RestaurantService } from 'src/app/services/restaurant.service';
import { Router } from '@angular/router';
import { Menu } from '../../models/restaurant/menu';
import { AuthenticationService } from 'src/app/services/authentication.service';
import {HttpErrorResponse} from '@angular/common/http';
import {OpeningHour} from '../../models/restaurant/opening-hour';
import {Time} from '@angular/common';
import {Day} from '../../enums/day.enum';

@Component({
  selector: 'app-add-restaurant',
  templateUrl: './add-restaurant.component.html',
  styleUrls: ['./add-restaurant.component.scss']
})
export class AddRestaurantComponent implements OnInit {
  private previousSelected;
  public isImageSelected = false;
  public showLoading = false;
  public restaurant: Restaurant;
  public address: Address;
  public menu: Menu;

  // lists
  public keys = Object.keys;
  public types = KitchenType;
  constructor(private restaurantService: RestaurantService, private authenticationService: AuthenticationService,
              private router: Router) { }
  ngOnInit(): void {
    if (this.restaurant == null) {
      this.restaurant = new Restaurant();
      this.restaurant.parking = false;
    }
    this.address = new Address();
    this.menu = new Menu();
  }
  // check or uncheck selected image
  checkImage(event): void {
    const classes = event.target.classList;
    // check or uncheck the image
    if (classes.contains('show')) {
      classes.remove('show');
      this.restaurant.image = '';
      this.isImageSelected = false;
    } else {
      // check if other image is checked when new image is checked previous will be remove the check
      if (this.previousSelected != null) {
        this.previousSelected.classList.remove('show');
        this.isImageSelected = false;
      }
      this.previousSelected = event.target;
      classes.add('show');
      this.isImageSelected = true;
      this.restaurant.image = event.target.id;
    }
  }
  handleForm(): void {
    this.restaurant.address = this.address;
    this.restaurant.user = this.authenticationService.getUserFromLocalCache();
    console.log(this.restaurant);
    this.showLoading = true;
    this.restaurantService.saveRestaurant(this.restaurant).subscribe((response: Restaurant) => {
      this.showLoading = false;
      this.router.navigate(['restaurant/menu/add', response.id]);
    },
      (errorResponse: HttpErrorResponse) => {
        if (errorResponse instanceof HttpErrorResponse) {
          if (errorResponse.error instanceof ErrorEvent) {
            console.error('Error event');
          } else {
            switch (errorResponse.status) {
              case 403:
                this.router.navigateByUrl('/error/403');
                break;
              case 401:
                this.router.navigateByUrl('/login');
                break;
              case 404:
                this.router.navigate(['**']);
                break;
              case 500:
                this.router.navigateByUrl('/error/500');
            }
          }
        }
        this.showLoading = true;
        console.log(errorResponse.error.message);
      });
  }

  arrayOne(n: number): any[] {
    return Array(n);
  }
}
