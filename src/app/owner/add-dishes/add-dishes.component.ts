import {Component, Input, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Dish} from '../../models/dish/dish';
import {HttpErrorResponse} from '@angular/common/http';
import {Menu} from '../../models/restaurant/menu';
import {MenuService} from '../../services/menu.service';
import {DishType} from '../../enums/dish-type.enum';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-dishes',
  templateUrl: './add-dishes.component.html',
  styleUrls: ['./add-dishes.component.scss']
})
export class AddDishesComponent implements OnInit {
  @Input() menu: Menu;

  public isListLoading = false;
  public showLoading = false;
  public editDishId: number;

  public dish: Dish;
  public dishes: Dish[] = [];
  public editDish: Dish;
  public keys = Object.keys;
  public dishTypes = DishType;
  public dishToRemove: Dish = new Dish();
  public showIsRemoved = false;
  public message = '';
  public showError: boolean;



  constructor(private menuService: MenuService, private router: Router) {
    this.dish = new Dish(false, false, false);
    this.editDish = new Dish();
  }

  ngOnInit(): void {
    this.loadDishes();
  }

  // METHODS FOR DISHES
  addDish(form: NgForm): void {
    this.showLoading = true;
    this.menuService.saveDish(this.dish, this.menu.id).subscribe((dish: Dish) => {
        this.showLoading = false;
        this.loadDishes();
      },
      (errorResponse: HttpErrorResponse) => {
          if (errorResponse.error instanceof ErrorEvent) {
            console.error('Error event');
          } else {
            switch (errorResponse.status) {
              case 403:
                this.router.navigateByUrl('/error/403');
                break;
              case 401:
                this.router.navigateByUrl('/login');
                break;
              case 404:
                this.router.navigate(['**']);
                break;
              case 500:
                this.router.navigateByUrl('/error/500');
            }
          }
          console.log(errorResponse.error.message);
          this.showLoading = false;
      });
    form.reset();
    // Reset the dish
    this.dish = new Dish(false, false, false);
  }

  removeDish(): void {
    this.menuService.removeDish(this.dishToRemove.id, this.menu.id).subscribe(data => {
      this.loadDishes();
      this.showInfoMessage(this.dishToRemove.name + ' is verwijderd van het menu');
    },
      (errorResponse: HttpErrorResponse) => {
          if (errorResponse.error instanceof ErrorEvent) {
            console.error('Error event');
          } else {
            switch (errorResponse.status) {
              case 403:
                this.router.navigateByUrl('/error/403');
                break;
              case 401:
                this.router.navigateByUrl('/login');
                break;
              case 404:
                this.router.navigate(['**']);
                break;
              case 500:
                this.router.navigateByUrl('/error/500');
            }
          }
          console.log(errorResponse.error.message);
      });
  }

  loadDishes(): void {
    if (this.menu !== undefined) {
      this.isListLoading = true;
      this.menuService.loadDishes(this.menu.id).subscribe( (data: Dish[]) => {
          this.dishes = data;
          this.isListLoading = false;
        },
        (errorResponse: HttpErrorResponse) => {
            if (errorResponse.error instanceof ErrorEvent) {
              console.error('Error event');
            } else {
              switch (errorResponse.status) {
                case 403:
                  this.router.navigateByUrl('/error/403');
                  break;
                case 401:
                  this.router.navigateByUrl('/login');
                  break;
                case 404:
                  this.router.navigate(['**']);
                  break;
                case 500:
                  this.router.navigateByUrl('/error/500');
              }
            }
            console.log(errorResponse.error.message);
            this.isListLoading = false;
        });
    }
  }

  onUpdateDish(dish: Dish): void {
    this.editDish.name = dish.name;
    this.editDish.dishType = dish.dishType;
    this.editDish.price = dish.price;
    this.editDish.vegan = dish.vegan;
    this.editDish.vegetarian = dish.vegetarian;
    this.editDish.containsNuts = dish.containsNuts;
    this.editDishId = dish.id;
    this.editDish.id = dish.id;
    this.editDish.menu = this.menu;
    console.log(this.editDish);
    console.log(dish.id);
  }

  saveUpdate(): void {
    this.menuService.updateDish(this.editDish).subscribe((data: Dish) => {
      this.loadDishes();
    },
      (errorResponse: HttpErrorResponse) => {
        console.log(errorResponse.error.message);
      });
  }

  getType(type: string): string {
    switch (type) {
      case 'STARTER':
        return '';
      case 'MAIN':
        return 'Hoofdgerecht';
      case 'DESSERT':
        return 'Dessert';
      case 'OTHER':
        return 'Andere';
    }
  }

  setToBeRemoved(item: Dish): void {
    this.dishToRemove = item;
  }

  private showInfoMessage(message: string): void {
    this.showIsRemoved = true;
    this.message = message;
    // show message for 3 seconds
    setTimeout(() => {
      this.showIsRemoved = false;
    }, 3000);
  }

  private showErrorMessage(message: string): void {
    this.showError = true;
    this.message = message;
    // show message for 3 seconds
    setTimeout(() => {
      this.showError = false;
    }, 3000);
  }
}
