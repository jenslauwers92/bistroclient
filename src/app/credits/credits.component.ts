import { Component, OnInit } from '@angular/core';
import { AuthenticationService} from '../services/authentication.service';
import {AccountDetailsService} from '../services/account-details.service';
import {NgForm} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {AccountDetails} from '../models/accountDetails/account-details';
import {UserService} from '../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-credits',
  templateUrl: './credits.component.html',
  styleUrls: ['./credits.component.scss']
})
export class CreditsComponent implements OnInit {
  constructor(public authentication: AuthenticationService, public accountDetails: AccountDetailsService
              , public userService: UserService,public router: Router) { }
  user = this.authentication.getUserFromLocalCache();

  userDetails = this.user.accountDetails;
  //credits = this.authentication.getUserFromLocalCache().accountDetails.credits;
  userName = this.user.username;
  refreshedUserDetails: AccountDetails;
  addition: number;
  credits: number;

  processForm(addForm: NgForm): void {
    console.log(this.credits);
    this.convertPositive();
    this.accountDetails.updateCredits( this.userName, this.addition).subscribe(
      ( accountDetails: AccountDetails) => {
        console.log(accountDetails); addForm.reset(); }, (errorResponse: HttpErrorResponse) => {
        console.log(errorResponse);
      }
    );
    location.reload();
    // this.router.navigateByUrl('/account/credits', { skipLocationChange: true }).then(() => {
    //   this.router.navigate(['/account/credits']);
    // });
  }
  ngOnInit(): void {
    this.getUserDetails();
    this.userDetails = this.user.accountDetails;
    this.userName = this.user.username;
  }
  convertPositive(): void {
    if (this.addition.toString().charAt(0) === '-'){
      this.addition = -(this.addition);
    }
  }

  getUserDetails(): void {
    this.accountDetails.getCurrentAccountDetail(this.userName).subscribe(refreshedUserDetails => { // subscribe voert methode uit
      this.credits = refreshedUserDetails.credits;
      console.log(this.refreshedUserDetails);
    });
  }

}
